<?php

namespace App\Exception;

final class ImageCategoryNotFoundException extends \Exception
{
}