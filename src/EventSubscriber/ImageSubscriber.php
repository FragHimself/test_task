<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Entity\Image;
use App\Entity\User;
use App\DBAL\EnumImageCategoryType;
use App\Exception\ImageCategoryNotFoundException;

final class ImageSubscriber implements EventSubscriberInterface
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                'checkIsImageCategoryExists', EventPriorities::PRE_VALIDATE,
                'attachOwnerToImage', EventPriorities::PRE_WRITE
            ]
        ];
    }

    public function attachOwnerToImage(ViewEvent $event)
    {
        $image = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$image instanceof Image || Request::METHOD_POST !== $method) {
            return;
        }

        $token = $this->tokenStorage->getToken();
        if (!$token) {
            return;
        }

        $user = $token->getUser();
        if (!$user instanceof User) {
            return;
        }

        $image->setUser($user);
    }

    public function checkIsImageCategoryExists(ViewEvent $event): void
    {
        $image = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        if (!$image instanceof Image || Request::METHOD_POST !== $method) {
            return;
        }

        $category = $image->getCategory();
        if (!in_array($category, array(EnumImageCategoryType::CATEGORY_NEW, EnumImageCategoryType::CATEGORY_POPULAR))) {
            throw new ImageCategoryNotFoundException(sprintf('Invalid image category - "%s"', $category));
        }
    }
}